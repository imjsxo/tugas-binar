var http = require('http'); //menggunakan library eksternal namanya 'http'
var moment = require('moment');
moment.locale('id');
const port = 8080

http.createServer(function (req, res) { //bikin server yang siap terima request
res.writeHead(200, {'Content-Type': 'text/html'}); //header HTTP request
res.write('Sekarang hari '+moment().format('dddd')+'\n');
res.write('Pukul '+moment().format('LT')+'\n');
res.write('Tanggal '+moment().format('LL'));
res.end(); //isi pesan
}).listen(port); //bisa diakses di port 8080 